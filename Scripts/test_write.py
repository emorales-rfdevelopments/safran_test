# Test Write Script
# Copyright (C) 2024  Emilio Morales

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import os
import sys

import time
import traceback
from threading import Thread

import tango


def WriteValues(device_proxy, min, max, sleep=0.1):
    try:
        _attrs = [a.name for a in device_proxy.attribute_list_query() if a.name not in ["Status", "State"]]
        for r in range(min, max, 1):
            for attr in _attrs:
                print("Writing value {} at attr {}".format(r, attr))
                device_proxy.write_attribute(attr, int(r))
            time.sleep(sleep)
    except Exception as e:
        print("Problems in WriteValues method: {}\n{}".format(e, traceback.format_exc()))

def main():

    dev_name = "safran/test/yaml"
    dp = tango.DeviceProxy(dev_name)

    for i in range(10000):
        WriteValues(dp, 1, 11)



if __name__ in ["__main__"]:

    print("Starting...")
    start_time = time.time()
    myThreadList = []

    # Create 10 Threads:
    for i in range(10):
        myThread = Thread(target=main)
        myThreadList.append(myThread)

    # Start Threads:
    for item in myThreadList:
        item.start()

    # Wait until all Threads end:
    for item in myThreadList:
        item.join()

    end_time = time.time() - start_time

    print("Elapsed time: {}".format(end_time))