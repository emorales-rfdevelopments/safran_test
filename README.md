# Safran LLRF Test Scritps
Project to store Safran Tango devices to test if hardware perform as expected. Test results [here](https://confluence.cells.es/display/CS/SAFRAN+LLRF+HARDWARE+TEST+II)

## Scripts

### test_read.py
Script that reads whole Tango device attributes form a device server every 0.1 seconds 10000 times.
Using threading, we simmulate that 10 clients are doing this job at the same time.

### test_read.py
Script that write whole Tango device attributes form a device server every 0.1 seconds 10000 times.
Using threading, we simmulate that 10 clients are doing this job at the same time.

## YAML files

### description.yaml
File generated at ALBA to perform tests with demo hardware.

## About
* Author: E.Morales (emorales@cells.es)
* Copyright: Copyright 2023, CELLS / ALBA Synchrotron